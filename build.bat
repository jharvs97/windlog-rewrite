@echo off
if not exist .\bin mkdir bin

xcopy /y data bin\data\

set EntryPoint=..\code\main.cpp
set CompilerFlags=-MTd -FC -Fe"windlog.exe" -Fo"windlog.obj" -W4 -wd4100 -GR- -Zo -Zi -O2 -Oi -fp:fast -fp:except-
set LinkerFlags=-DEBUG -INCREMENTAL:NO -OPT:REF -SUBSYSTEM:CONSOLE
set LinkerLibs=kernel32.lib user32.lib gdi32.lib shell32.lib

pushd bin
cl %EntryPoint% %CompilerFlags% -link %LinkerFlags% %LinkerLibs%
popd bin