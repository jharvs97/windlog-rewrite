#include <windows.h>
#include <strsafe.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <intrin.h>

#define internal static
#define global_variable static
#define Kilobytes(Bytes) Bytes * 1024u
#define Megabytes(Bytes) Bytes * 1024u * 1024u

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float f32;
typedef size_t usize;

global_variable HANDLE GlobalStdoutHandle;
global_variable HANDLE GlobalStdinHandle;

struct date
{
    u8 Day;
    u8 Month;
    u16 Year;
    u8 Hour;
    u8 Minute;
};

struct string_view
{
    char* Begin;
    char* End;
};

struct csv_data_header_locations
{
    s32 HeaderCount;
    u8 WAST;
    u8 S;
    u8 SR;
    u8 T;
};

struct mean_stdev_of_range
{
    f32 Mean;
    f32 StandardDeviation;
};

struct per_month_mean_stdev
{
    mean_stdev_of_range Months[12];
};

struct linear_allocator
{
    void *Memory;
    void *Head;
    usize SizeInBytes;
    usize BytesWritten;
    usize BytesCommitted;
    usize PageSize;
};

struct wind_data_buffer
{
    f32 *SolarRadiations;
    f32 *WindSpeeds;
    f32 *Temperatures;
    date *Dates;
    
    usize Count;
    usize TailIndex;
};

internal string_view
PopWordByDelimiter(string_view *StringView, char Delimiter)
{
    string_view Result = *StringView;
    
    for (char* C = StringView->Begin;
         C <= StringView->End;
         C++)
    {
        if (*C == 0)
        {
            break;
        }
        
        if (*C == Delimiter)
        {
            Result.End = C-1;
            StringView->Begin = C+1;
            break;
        }
    }
    
    return Result;
}


internal bool
IsValidMeanStDev(mean_stdev_of_range *Data)
{
    return (Data->Mean != 0 &&
            Data->StandardDeviation != 0);
}

internal linear_allocator
LinearAllocatorCreate(usize SizeInBytes)
{
    linear_allocator Arena = {};
    Arena.SizeInBytes = SizeInBytes;
    Arena.Memory = VirtualAlloc(0, SizeInBytes, MEM_RESERVE, PAGE_READWRITE);
    Arena.Head = Arena.Memory;
    Arena.BytesCommitted = 0;
    
    SYSTEM_INFO SystemInfo;
    GetSystemInfo(&SystemInfo);
    Arena.PageSize = SystemInfo.dwPageSize;
    
    return Arena;
}

internal void
LinearAllocatorReset(linear_allocator *Allocator)
{
    Allocator->Head = Allocator->Memory;
    Allocator->BytesWritten = 0;
}

internal void *
Allocate(linear_allocator *Allocator, usize SizeInBytes)
{
    usize BytesWrittenAfterAlloc = Allocator->BytesWritten + SizeInBytes;
    if (BytesWrittenAfterAlloc <= Allocator->SizeInBytes)
    {
        if (BytesWrittenAfterAlloc > Allocator->BytesCommitted)
        {
            usize NumberOfBytesToCommit = ((SizeInBytes / Allocator->PageSize) + 1) * Allocator->PageSize;
            VirtualAlloc(Allocator->Head, NumberOfBytesToCommit, MEM_COMMIT, PAGE_READWRITE);
            Allocator->BytesCommitted += NumberOfBytesToCommit;
        }
        
        void *Head = Allocator->Head;
        Allocator->Head = ((u8 *) Allocator->Head) + SizeInBytes;
        Allocator->BytesWritten += SizeInBytes;
        return Head;
    }
    else
    {
        return 0;
    }
}

internal void
LinearAllocatorDestroy(linear_allocator *Allocator)
{
    VirtualFree(Allocator->Memory, Allocator->SizeInBytes, MEM_RELEASE);
}

internal const char *
MonthS32ToString(s32 Month)
{
    switch (Month)
    {
        case 1: return "January";
        case 2: return "February";
        case 3: return "March";
        case 4: return "April";
        case 5: return "May";
        case 6: return "June";
        case 7: return "July";
        case 8: return "August";
        case 9: return "September";
        case 10: return "October";
        case 11: return "November";
        case 12: return "December";
    }
    
    return "";
}

internal usize
StringViewLength(string_view SV)
{
    if (SV.Begin == SV.End && 
        *SV.Begin == 0)
    {
        return 0;
    }
    
    return (SV.End - SV.Begin) + 1;
}

internal s32
StringViewToS32(string_view IntSV)
{
    usize Length = StringViewLength(IntSV);
    char Buffer[1024] = {};
    for (usize I = 0; I < Length; I++)
    {
        Buffer[I] = *(IntSV.Begin++);
    }
    
    s32 IntResult = atoi((const char *) Buffer);
    
    return IntResult;
}

internal f32
StringViewToF32(string_view FloatSV)
{
    usize Length = StringViewLength(FloatSV);
    constexpr usize BufferSize = 2048;
    if (Length >= BufferSize)
    {
        return 0.0f;
    }
    char Buffer[BufferSize] = {};
    for (usize I = 0; I < Length; I++)
    {
        Buffer[I] = *(FloatSV.Begin++);
    }
    
    f32 FloatResult = (f32) atof((const char *) Buffer);
    
    return FloatResult;
}

internal date
StringViewToDate(string_view DateSV)
{
    
    string_view Day = PopWordByDelimiter(&DateSV, '/');
    string_view Month = PopWordByDelimiter(&DateSV, '/');
    string_view Year = PopWordByDelimiter(&DateSV, ' ');
    string_view Hour = PopWordByDelimiter(&DateSV, ':');
    string_view Minute = DateSV;
    
    s32 Minutes = ((StringViewToS32(Hour) * 60) +  StringViewToS32(Minute));
    
    if (Minutes < 0 || Minutes > 24 * 60)
    {
        Minutes = 0;
    }
    
    date Date = {};
    Date.Day = (u8) StringViewToS32(Day);
    Date.Month = (u8) StringViewToS32(Month);
    Date.Year = (u16) StringViewToS32(Year);
    Date.Hour = (u8) StringViewToS32(Hour);
    Date.Minute = (u8) StringViewToS32(Minute);
    return Date;
}


internal bool
StringViewCompareToCStr(string_view Left, const char* Right)
{
    if (StringViewLength(Left) == lstrlenA(Right))
    {
        usize Length = StringViewLength(Left);
        
        for (usize I = 0; I < Length; I++)
        {
            char LeftChar = *(Left.Begin + I);
            char RightChar = Right[I];
            
            if (LeftChar != RightChar)
            {
                return false;
            }
        }
        
        return true;
        
    }
    
    return false;
}

internal string_view
PopLine(string_view *StringView)
{
    string_view Result = *StringView;
    
    for (char* C = StringView->Begin;
         C <= StringView->End;
         C++)
    {
        if (*C == 0)
        {
            break;
        }
        
        // TODO: Cross-platform this
        if (*C == '\r' && *(C+1) == '\n')
        {
            Result.End = C-1;
            StringView->Begin = C+2;
            break;
        }
    }
    
    return Result;
}

internal void
WriteFormatStringToOutputHandle(HANDLE OutHandle, char* FormatString, ...)
{
    va_list Args;
    va_start(Args, FormatString);
    
    char Buffer[2048] = {};
    HRESULT FormatResult = StringCbVPrintfA(Buffer, 2048, FormatString, Args);
    
    if (FormatResult == S_OK)
    {
        DWORD WrittenBytes = 0;
        WriteFile(OutHandle, Buffer, (DWORD) lstrlenA(Buffer), &WrittenBytes, 0);
    }
    
    va_end(Args);
}

internal void
WriteStringToOutputHandle(HANDLE OutHandle, char* StringToWrite)
{
    DWORD WrittenBytes = 0;
    WriteFile(OutHandle, StringToWrite, (DWORD) lstrlenA(StringToWrite), &WrittenBytes, 0);
}

internal s32 ReadFromInputHandleIntoBuffer(HANDLE InHandle, char* OutBuffer, usize BufferSize)
{
    DWORD CharsRead = 0;
    ReadFile(InHandle,
             (void*) OutBuffer,
             (DWORD) BufferSize,
             &CharsRead,
             0);
    
    return CharsRead;
}

internal string_view
StringToStringView(char *Buffer, usize StringLength)
{
    string_view Result;
    Result.Begin = Buffer;
    Result.End = Buffer + StringLength;
    
    return Result;
}

internal s32
ReadS32FromInputHandle(HANDLE InHandle)
{
    char Buffer[1024] = {};
    s32 CharsRead = ReadFromInputHandleIntoBuffer(InHandle, Buffer, 1024);
    
    string_view BufferSV = StringToStringView(Buffer, CharsRead);
    s32 Value = StringViewToS32(BufferSV);
    
    return Value;
}

internal void
Win32InitializeConsoleHandles()
{
    GlobalStdoutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
    GlobalStdinHandle = GetStdHandle(STD_INPUT_HANDLE);
    
    if (GlobalStdoutHandle == INVALID_HANDLE_VALUE ||
        GlobalStdinHandle  == INVALID_HANDLE_VALUE)
    {
        // TODO: Logging
    }
}

internal u32
CountNumberOfLinesInFile(string_view DataSV)
{
    u32 SampleCount = 0;
    for (char *C = DataSV.Begin;
         C <= DataSV.End;
         C++)
    {
        if (*C == '\n')
        {
            SampleCount++;
        }
    }
    
    return SampleCount;
}

internal csv_data_header_locations
ParseCSVHeader(string_view Header)
{
    char *WordBegin = Header.Begin;
    u8 WordIndex = 0;
    csv_data_header_locations Locations = {};
    
    for (char *C = Header.Begin;
         C <= Header.End;
         C++)
    {
        if (Header.End - C == 0 || *(C+1) == ',')
        {
            string_view ColumnHeader;
            ColumnHeader.Begin = WordBegin;
            ColumnHeader.End = C;
            
            if (StringViewCompareToCStr(ColumnHeader, "WAST"))
            {
                Locations.WAST = WordIndex;
            }
            else if (StringViewCompareToCStr(ColumnHeader, "S"))
            {
                Locations.S = WordIndex;
            }
            else if (StringViewCompareToCStr(ColumnHeader, "SR"))
            {
                Locations.SR = WordIndex;
            }
            else if (StringViewCompareToCStr(ColumnHeader, "T"))
            {
                Locations.T = WordIndex;
            }
            
            WordIndex++;
            WordBegin = C+2;
        }
    }
    
    Locations.HeaderCount = WordIndex;
    
    return Locations;
}

internal bool
GetModuleFileDirectory(char *ModuleFileName, usize ModuleFileNameLength, char *DestBuffer, usize DestBufferSize)
{
    u32 LastSlashIndex = 0;
    
    for (u32 I = 0; I < ModuleFileNameLength; I++)
    {
        if (ModuleFileName[I] == '\\')
        {
            LastSlashIndex = I;
        }
    }
    
    if (LastSlashIndex >= DestBufferSize)
    {
        return false;
    }
    
    CopyMemory((LPVOID) DestBuffer,
               (LPVOID) ModuleFileName,
               LastSlashIndex);
    
    DestBuffer[LastSlashIndex] = 0;
    
    return true;
}

internal string_view
ReadEntireFile(linear_allocator *Allocator, const char *Filename)
{
    string_view FileSV = {};
    HANDLE DataFileHandle = CreateFileA(Filename,
                                        GENERIC_READ,
                                        0,
                                        0,
                                        OPEN_EXISTING,
                                        FILE_ATTRIBUTE_NORMAL,
                                        0);
    
    if (DataFileHandle != INVALID_HANDLE_VALUE)
    {
        char *FileDataBuffer = 0;
        usize FileDataBufferSize = Megabytes(8);
        FileDataBuffer = (char *) Allocate(Allocator, FileDataBufferSize);
        
        // TODO: Log if not enough memory to read entire file.
        DWORD BytesRead = 0;
        ReadFile(DataFileHandle,
                 FileDataBuffer,
                 (DWORD) FileDataBufferSize,
                 &BytesRead,
                 0);
        
        FileDataBuffer[BytesRead] = 0;
        
        if (!CloseHandle(DataFileHandle))
        {
            // TODO: Logging
        }
        
        
        FileSV.Begin = FileDataBuffer;
        FileSV.End   = FileDataBuffer + BytesRead;
    }
    return FileSV;
}

// TODO: Subsequent calls should do an append + inplace sort.
internal void
LoadCSVDataAndAppendToWindDataBuffer(linear_allocator Allocator, const char *Filename, wind_data_buffer *WindDataBuffer)
{
    string_view FileSV = ReadEntireFile(&Allocator, Filename);
    u32 NumberOfLines = 0;
    if (StringViewLength(FileSV) > 0)
    {
        string_view HeaderLine = PopLine(&FileSV);
        csv_data_header_locations HeaderDataLocations = ParseCSVHeader(HeaderLine);
        
        NumberOfLines = CountNumberOfLinesInFile(FileSV);
        
        for (u32 LineIndex = 0; LineIndex < NumberOfLines; LineIndex++)
        {
            string_view Line = PopLine(&FileSV);
            
            if (StringViewLength(Line) == 0)
            {
                break;
            }
            
            for (s32 WordIndex = 0; WordIndex < HeaderDataLocations.HeaderCount; WordIndex++)
            {
                string_view Word = PopWordByDelimiter(&Line, ',');
                
                usize Length = StringViewLength(Word);
                
                if (Length == 0)
                {
                    continue;
                }
                
                if (WordIndex == HeaderDataLocations.WAST)
                {
                    WindDataBuffer->Dates[WindDataBuffer->TailIndex] = StringViewToDate(Word);
                }
                else if (WordIndex == HeaderDataLocations.S)
                {
                    WindDataBuffer->WindSpeeds[WindDataBuffer->TailIndex] = StringViewToF32(Word);
                }
                else if (WordIndex == HeaderDataLocations.SR)
                {
                    WindDataBuffer->SolarRadiations[WindDataBuffer->TailIndex] = StringViewToF32(Word);
                }
                else if (WordIndex == HeaderDataLocations.T)
                {
                    WindDataBuffer->Temperatures[WindDataBuffer->TailIndex] = StringViewToF32(Word);
                }
            }
            
            WindDataBuffer->TailIndex++;
        }
    }
}

internal mean_stdev_of_range
CalcAverageAndStDevOfRange(f32 *Data, usize Count)
{
    usize Width = 8;
    usize CountModWidth = Count % Width;
    usize Watermark = Count -  CountModWidth;
    
    mean_stdev_of_range Result = {};
    
    __m256 TotalForAvg_8f = _mm256_set1_ps(0.0f);
    
    for (usize I = 0;
         I < Watermark;
         I += Width)
    {
        __m256 Values = _mm256_load_ps(&Data[I]);
        TotalForAvg_8f = _mm256_add_ps(TotalForAvg_8f, Values);
    }
    
    // TODO: Better reduce
    f32 TotalForMean = 0.0f;
    for (u32 I = 0; I < Width; I++) TotalForMean += TotalForAvg_8f.m256_f32[I];
    
    if (CountModWidth > 0)
    {
        for (usize I = Watermark; I < Count; I++)
        {
            TotalForMean += Data[I];
        }
    }
    
    f32 Mean = TotalForMean / Count;
    
    __m256 Mean_8f = _mm256_set1_ps(Mean);
    __m256 TotalForStDev_8f = _mm256_set1_ps(0.0f);
    
    for (usize I = 0;
         I < Watermark;
         I += Width)
    {
        __m256 Values = _mm256_load_ps(&Data[I]);
        __m256 DataMinusMean_8f = _mm256_sub_ps(Values, Mean_8f);
        __m256 SqDataMinusMean_8f = _mm256_mul_ps(DataMinusMean_8f, DataMinusMean_8f);
        __m256 AbsDataMinusAvg_8f = _mm256_sqrt_ps(SqDataMinusMean_8f);
        TotalForStDev_8f = _mm256_add_ps(TotalForStDev_8f, AbsDataMinusAvg_8f);
    }
    
    // TODO: Better reduce?
    f32 TotalForStDev = 0.0f;
    for (u32 I = 0; I < Width; I++) TotalForStDev += TotalForStDev_8f.m256_f32[I];
    
    if (CountModWidth > 0)
    {
        for (usize I = Watermark; I < Count; I++)
        {
            f32 DataMinusMean = Data[I] - Mean;
            f32 SqDataMinusMean = DataMinusMean * DataMinusMean;
            f32 AbsDataMinusMean = (f32) sqrt(SqDataMinusMean);
            TotalForStDev += AbsDataMinusMean;
        }
    }
    
    f32 StDev = TotalForStDev / Count;
    
    Result.Mean = Mean;
    Result.StandardDeviation = StDev;
    
    return Result;
}

internal mean_stdev_of_range
CalcDataForMonthAndYear(f32 *Data, date *Dates, usize Count, s32 Month, s32 Year)
{
    // NOTE: Find start index
    s64 StartIndex = -1;
    s64 EndIndex = Count+1;
    
    for (usize I = 0; I < Count; I++)
    {
        date Date = Dates[I];
        if (Year == Date.Year &&
            Month == Date.Month)
        {
            StartIndex = I;
            break;
        }
    }
    
    // NOTE: Find end index
    for (usize I = StartIndex; I < Count; I++)
    {
        date Date = Dates[I];
        if (Year != Date.Year ||
            Month != Date.Month)
        {
            EndIndex = I;
            break;
        }
    }
    
    if (StartIndex == -1 || EndIndex == (s64)(Count + 1))
    {
        return {};
    }
    
    mean_stdev_of_range Result = CalcAverageAndStDevOfRange(Data + StartIndex, EndIndex - StartIndex);
    
    return Result;
}

internal per_month_mean_stdev
CalcDataForEachMonthOfYear(f32 *Data, date *Dates, usize Count, s32 Year)
{
    per_month_mean_stdev PerMonthData = {};
    u32 BeginningIndex = 0;
    
    // NOTE: Assumes data is sorted
    for (u32 I = 0; I < Count; I++)
    {
        date Date = Dates[I];
        if (Year == Date.Year)
        {
            BeginningIndex = I;
            break;
        }
    }
    
    u32 CurrentMonth = Dates[BeginningIndex].Month;
    u32 SamplesTaken = 0;
    u32 BeginningSampleIndex = BeginningIndex;
    
    // NOTE: For all months leading up to the start month, print "no data"
    
    for (u32 I = BeginningIndex; 
         I < Count && Dates[I].Year == Year; 
         I++)
    {
        date Date = Dates[I];
        
        if (Date.Month != CurrentMonth)
        {
            if (SamplesTaken != 0)
            {
                mean_stdev_of_range Result = CalcAverageAndStDevOfRange(Data + BeginningSampleIndex, SamplesTaken);
                PerMonthData.Months[CurrentMonth - 1] = Result;
                
                CurrentMonth = Date.Month;
                BeginningSampleIndex = I;
                SamplesTaken = 0;
            }
        }
        else
        {
            SamplesTaken++;
        }
    }
    
    //NOTE: If we have left over valid samples, calculate!
    if (SamplesTaken > 0)
    {
        mean_stdev_of_range Result = CalcAverageAndStDevOfRange(Data + BeginningSampleIndex, SamplesTaken);
        PerMonthData.Months[CurrentMonth - 1] = Result;
    }
    
    return PerMonthData;
}

internal void
Win32SetWorkingDirectoryToExePath()
{
    // NOTE: Change directory to EXE directory
    char ModuleFileName[256] = {};
    
    GetModuleFileNameA(0, ModuleFileName, 256);
    
    char ModuleDirectory[256] = {};
    if (GetModuleFileDirectory(ModuleFileName, sizeof(ModuleFileName), ModuleDirectory, sizeof(ModuleDirectory)))
    {
        WriteFormatStringToOutputHandle(GlobalStdoutHandle, "Module path is: %s\n", ModuleDirectory);
        if (!SetCurrentDirectoryA(ModuleDirectory))
        {
            WriteStringToOutputHandle(GlobalStdoutHandle, "Couldn't set CWD!\n");
        }
    }
}

int main(int ArgCount, char** Args)
{
    Win32InitializeConsoleHandles();
    Win32SetWorkingDirectoryToExePath();
    
    wind_data_buffer WindDataBuffer = {};
    WindDataBuffer.Count = 1000000u; // Amount of records
    
    linear_allocator Allocator = LinearAllocatorCreate(Megabytes(50));
    
    WindDataBuffer.SolarRadiations = (f32 *)  Allocate(&Allocator, WindDataBuffer.Count * sizeof(f32));
    WindDataBuffer.WindSpeeds      = (f32 *)  Allocate(&Allocator, WindDataBuffer.Count * sizeof(f32));
    WindDataBuffer.Temperatures    = (f32 *)  Allocate(&Allocator, WindDataBuffer.Count * sizeof(f32));
    WindDataBuffer.Dates           = (date *) Allocate(&Allocator, WindDataBuffer.Count * sizeof(date));
    
    const char* File2017 = ".\\data\\01Jan2017-01Jan2018.csv";
    LoadCSVDataAndAppendToWindDataBuffer(Allocator, File2017, &WindDataBuffer);
    const char* File2018 = ".\\data\\01Apr2018-01Apr2019.csv";
    LoadCSVDataAndAppendToWindDataBuffer(Allocator, File2018, &WindDataBuffer);
    const char* File2019 = ".\\data\\01May2019-01May2020.csv";
    LoadCSVDataAndAppendToWindDataBuffer(Allocator, File2019, &WindDataBuffer);
    
    bool ShouldClose = false;
    while (!ShouldClose)
    {
        WriteStringToOutputHandle(GlobalStdoutHandle, "Choose an option using numbers 1-4\n");
        WriteStringToOutputHandle(GlobalStdoutHandle, "1. Average wind speed of a month and year\n");
        WriteStringToOutputHandle(GlobalStdoutHandle, "2. Average ambient air temperature for each month in a year\n");
        WriteStringToOutputHandle(GlobalStdoutHandle, "3. Total solar radiation in kWh/m^2 for each month in a year\n");
        WriteStringToOutputHandle(GlobalStdoutHandle, "4. Average wind speed (km/h), average ambient air temperature and total solar radiation in kWh/m^2 for each month in a year\n");
        WriteStringToOutputHandle(GlobalStdoutHandle, "5. Quit\n");
        
        s32 ChosenOption = ReadS32FromInputHandle(GlobalStdinHandle);
        
        switch (ChosenOption)
        {
            case 1:
            {
                WriteStringToOutputHandle(GlobalStdoutHandle, "Enter Month (MM): ");
                s32 Month = ReadS32FromInputHandle(GlobalStdinHandle);
                
                WriteStringToOutputHandle(GlobalStdoutHandle, "Enter Year (YYYY): ");
                s32 Year = ReadS32FromInputHandle(GlobalStdinHandle);
                
                mean_stdev_of_range Result = CalcDataForMonthAndYear(WindDataBuffer.WindSpeeds, WindDataBuffer.Dates, WindDataBuffer.TailIndex, Month, Year);
                
                if (IsValidMeanStDev(&Result))
                {
                    
                    WriteFormatStringToOutputHandle(GlobalStdoutHandle, "\n%s %d\n", MonthS32ToString(Month), Year);
                    WriteFormatStringToOutputHandle(GlobalStdoutHandle, "Average speed: %.02f km/h\n", Result.Mean);
                    WriteFormatStringToOutputHandle(GlobalStdoutHandle, "Sample stdev: %.02f\n\n", Result.StandardDeviation);
                }
                else
                {
                    WriteStringToOutputHandle(GlobalStdoutHandle, "\nNo data\n");
                }
            } break;
            case 2: 
            {
                WriteStringToOutputHandle(GlobalStdoutHandle, "Enter Year (YYYY): ");
                s32 Year = ReadS32FromInputHandle(GlobalStdinHandle);
                
                per_month_mean_stdev PerMonthData = CalcDataForEachMonthOfYear(WindDataBuffer.Temperatures, WindDataBuffer.Dates, WindDataBuffer.TailIndex, Year);
                
                for (u32 I = 0; I < 12; I++)
                {
                    mean_stdev_of_range Data = PerMonthData.Months[I];
                    if (IsValidMeanStDev(&Data))
                    {
                        WriteFormatStringToOutputHandle(GlobalStdoutHandle, "%s: average: %.02f degrees C, stdev: %.02f\n", MonthS32ToString(I+1), Data.Mean, Data.StandardDeviation);
                    }
                    else
                    {
                        WriteFormatStringToOutputHandle(GlobalStdoutHandle, "%s: No Data\n", MonthS32ToString(I+1));
                    }
                }
            } break;
            case 3: 
            {
                WriteStringToOutputHandle(GlobalStdoutHandle, "Enter Year (YYYY): ");
                s32 Year = ReadS32FromInputHandle(GlobalStdinHandle);
                
                auto TempAllocator = Allocator;

                f32 *FilteredSolarRadiations = (f32 *) Allocate(&TempAllocator, sizeof(f32) * WindDataBuffer.TailIndex);
                date *FilteredDates = (date *) Allocate(&TempAllocator, sizeof(date) * WindDataBuffer.TailIndex);
                
                u32 SampleIndex = 0;
                for (u32 I = 0; I < WindDataBuffer.TailIndex; I++)
                {
                    f32 Sample = WindDataBuffer.SolarRadiations[I];
                    if (Sample >= 100)
                    {
                        FilteredSolarRadiations[SampleIndex] = Sample;
                        FilteredDates[SampleIndex] = WindDataBuffer.Dates[I];
                        SampleIndex++;
                    }
                }
                
                per_month_mean_stdev PerMonthData = CalcDataForEachMonthOfYear(FilteredSolarRadiations, FilteredDates, SampleIndex, Year);
                
                for (u32 I = 0; I < 12; I++)
                {
                    mean_stdev_of_range Data = PerMonthData.Months[I];
                    if (IsValidMeanStDev(&Data))
                    {
                        f32 MeankWh = (Data.Mean * (1.0f/6.0f)) / 1000.0f;
                        f32 StDevkWh = (Data.StandardDeviation * (1.0f/6.0f)) / 1000.0f;
                        WriteFormatStringToOutputHandle(GlobalStdoutHandle, "%s: average: %.02f degrees C, stdev: %.02f\n", MonthS32ToString(I+1), MeankWh, StDevkWh);
                    }
                    else
                    {
                        WriteFormatStringToOutputHandle(GlobalStdoutHandle, "%s: No Data\n", MonthS32ToString(I+1));
                    }
                }
            }break;
            case 4: break;
            case 5:
            {
                ShouldClose = true;
            } break;
            default: break;
        }
    }
    
    LinearAllocatorDestroy(&Allocator);
    return 0;
}